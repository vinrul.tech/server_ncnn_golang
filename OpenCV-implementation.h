#ifdef __cplusplus
extern "C" {
#endif
void openVideo(char *filename);
int loadModel(char *fileParam, char *fileModel);
int loadImage(char *filename);
#ifdef __cplusplus
}
#endif
