#include <iostream>
#include <unistd.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

#include <string>
#include <vector>
#include <algorithm>
#include "layer.h"
#include "net.h"
#include "benchmark.h"

#include "OpenCV-implementation.h"

using namespace std;
using namespace cv;

static ncnn::UnlockedPoolAllocator g_blob_pool_allocator;
static ncnn::PoolAllocator g_workspace_pool_allocator;

static ncnn::Net yolov5;

struct DetectObject
{
   float x;
   float y;
   float w;
   float h;
   int label;
   float prob;
};

static inline float intersection_area(const DetectObject &a, const DetectObject &b)
{
   if (a.x > b.x + b.w || a.x + a.w < b.x || a.y > b.y + b.h || a.y + a.h < b.y)
   {
      // no intersection
      return 0.f;
   }

   float inter_width = min(a.x + a.w, b.x + b.w) - max(a.x, b.x);
   float inter_height = min(a.y + a.h, b.y + b.h) - max(a.y, b.y);

   return inter_width * inter_height;
}

static void qsort_descent_inplace(std::vector<DetectObject> &faceobjects, int left, int right)
{
   int i = left;
   int j = right;
   float p = faceobjects[(left + right) / 2].prob;

   while (i <= j)
   {
      while (faceobjects[i].prob > p)
         i++;

      while (faceobjects[j].prob < p)
         j--;

      if (i <= j)
      {
         // swap
         std::swap(faceobjects[i], faceobjects[j]);

         i++;
         j--;
      }
   }

#pragma omp parallel sections
   {
#pragma omp section
      {
         if (left < j)
            qsort_descent_inplace(faceobjects, left, j);
      }
#pragma omp section
      {
         if (i < right)
            qsort_descent_inplace(faceobjects, i, right);
      }
   }
}

static void qsort_descent_inplace(std::vector<DetectObject> &faceobjects)
{
   if (faceobjects.empty())
      return;

   qsort_descent_inplace(faceobjects, 0, faceobjects.size() - 1);
}

static void nms_sorted_bboxes(const std::vector<DetectObject> &faceobjects, std::vector<int> &picked, float nms_threshold)
{
   picked.clear();

   const int n = faceobjects.size();

   std::vector<float> areas(n);
   for (int i = 0; i < n; i++)
   {
      areas[i] = faceobjects[i].w * faceobjects[i].h;
   }

   for (int i = 0; i < n; i++)
   {
      const DetectObject &a = faceobjects[i];

      int keep = 1;
      for (int j = 0; j < (int)picked.size(); j++)
      {
         const DetectObject &b = faceobjects[picked[j]];

         // intersection over union
         float inter_area = intersection_area(a, b);
         float union_area = areas[i] + areas[picked[j]] - inter_area;
         // float IoU = inter_area / union_area
         if (inter_area / union_area > nms_threshold)
            keep = 0;
      }

      if (keep)
         picked.push_back(i);
   }
}

static inline float sigmoid(float x)
{
   return static_cast<float>(1.f / (1.f + exp(-x)));
}

static void generate_proposals(const ncnn::Mat &anchors, int stride, const ncnn::Mat &in_pad, const ncnn::Mat &feat_blob, float prob_threshold, std::vector<DetectObject> &objects)
{
   const int num_grid = feat_blob.h;

   int num_grid_x;
   int num_grid_y;
   if (in_pad.w > in_pad.h)
   {
      num_grid_x = in_pad.w / stride;
      num_grid_y = num_grid / num_grid_x;
   }
   else
   {
      num_grid_y = in_pad.h / stride;
      num_grid_x = num_grid / num_grid_y;
   }

   const int num_class = feat_blob.w - 5;

   const int num_anchors = anchors.w / 2;

   for (int q = 0; q < num_anchors; q++)
   {
      const float anchor_w = anchors[q * 2];
      const float anchor_h = anchors[q * 2 + 1];

      const ncnn::Mat feat = feat_blob.channel(q);

      for (int i = 0; i < num_grid_y; i++)
      {
         for (int j = 0; j < num_grid_x; j++)
         {
            const float *featptr = feat.row(i * num_grid_x + j);

            // find class index with max class score
            int class_index = 0;
            float class_score = -FLT_MAX;
            for (int k = 0; k < num_class; k++)
            {
               float score = featptr[5 + k];
               if (score > class_score)
               {
                  class_index = k;
                  class_score = score;
               }
            }

            float box_score = featptr[4];

            float confidence = sigmoid(box_score) * sigmoid(class_score);

            if (confidence >= prob_threshold)
            {
               // yolov5/models/yolo.py Detect forward
               // y = x[i].sigmoid()
               // y[..., 0:2] = (y[..., 0:2] * 2. - 0.5 + self.grid[i].to(x[i].device)) * self.stride[i]  # xy
               // y[..., 2:4] = (y[..., 2:4] * 2) ** 2 * self.anchor_grid[i]  # wh

               float dx = sigmoid(featptr[0]);
               float dy = sigmoid(featptr[1]);
               float dw = sigmoid(featptr[2]);
               float dh = sigmoid(featptr[3]);

               float pb_cx = (dx * 2.f - 0.5f + j) * stride;
               float pb_cy = (dy * 2.f - 0.5f + i) * stride;

               float pb_w = pow(dw * 2.f, 2) * anchor_w;
               float pb_h = pow(dh * 2.f, 2) * anchor_h;

               float x0 = pb_cx - pb_w * 0.5f;
               float y0 = pb_cy - pb_h * 0.5f;
               float x1 = pb_cx + pb_w * 0.5f;
               float y1 = pb_cy + pb_h * 0.5f;

               DetectObject obj;
               obj.x = x0;
               obj.y = y0;
               obj.w = x1 - x0;
               obj.h = y1 - y0;
               obj.label = class_index;
               obj.prob = confidence;

               objects.push_back(obj);
            }
         }
      }
   }
}

static int detect_yolov5(ncnn::Net &yolov5, const cv::Mat &bgr, std::vector<DetectObject> &objects, bool use_gpu)
{

   const int target_size = 640;
   const float prob_threshold = 0.4f;
   const float nms_threshold = 0.45f;

   int img_w = bgr.cols;
   int img_h = bgr.rows;

   // letterbox pad to multiple of 32
   int w = img_w;
   int h = img_h;
   float scale = 1.f;
   if (w > h)
   {
      scale = (float)target_size / w;
      w = target_size;
      h = h * scale;
   }
   else
   {
      scale = (float)target_size / h;
      h = target_size;
      w = w * scale;
   }

   ncnn::Mat in = ncnn::Mat::from_pixels_resize(bgr.data, ncnn::Mat::PIXEL_BGR2RGB, img_w, img_h, w, h);

   // pad to target_size rectangle
   // yolov5/utils/datasets.py letterbox
   int wpad = (w + 31) / 32 * 32 - w;
   int hpad = (h + 31) / 32 * 32 - h;
   ncnn::Mat in_pad;
   ncnn::copy_make_border(in, in_pad, hpad / 2, hpad - hpad / 2, wpad / 2, wpad - wpad / 2, ncnn::BORDER_CONSTANT, 114.f);

   const float norm_vals[3] = {1 / 255.f, 1 / 255.f, 1 / 255.f};
   in_pad.substract_mean_normalize(0, norm_vals);

   ncnn::Extractor ex = yolov5.create_extractor();

   //ex.set_vulkan_compute(use_gpu);

   ex.input("images", in_pad);

   std::vector<DetectObject> proposals;

   // anchor setting from yolov5/models/yolov5s.yaml

   // stride 8
   {
      ncnn::Mat out;
      ex.extract("output", out);

      ncnn::Mat anchors(6);
      anchors[0] = 10.f;
      anchors[1] = 13.f;
      anchors[2] = 16.f;
      anchors[3] = 30.f;
      anchors[4] = 33.f;
      anchors[5] = 23.f;

      std::vector<DetectObject> objects8;
      generate_proposals(anchors, 8, in_pad, out, prob_threshold, objects8);

      proposals.insert(proposals.end(), objects8.begin(), objects8.end());
   }

   // stride 16
   {
      ncnn::Mat out;
      ex.extract("375", out);

      ncnn::Mat anchors(6);
      anchors[0] = 30.f;
      anchors[1] = 61.f;
      anchors[2] = 62.f;
      anchors[3] = 45.f;
      anchors[4] = 59.f;
      anchors[5] = 119.f;

      std::vector<DetectObject> objects16;
      generate_proposals(anchors, 16, in_pad, out, prob_threshold, objects16);

      proposals.insert(proposals.end(), objects16.begin(), objects16.end());
   }

   // stride 32
   {
      ncnn::Mat out;
      ex.extract("400", out);

      ncnn::Mat anchors(6);
      anchors[0] = 116.f;
      anchors[1] = 90.f;
      anchors[2] = 156.f;
      anchors[3] = 198.f;
      anchors[4] = 373.f;
      anchors[5] = 326.f;

      std::vector<DetectObject> objects32;
      generate_proposals(anchors, 32, in_pad, out, prob_threshold, objects32);

      proposals.insert(proposals.end(), objects32.begin(), objects32.end());
   }

   // sort all proposals by score from highest to lowest
   qsort_descent_inplace(proposals);

   // apply nms with nms_threshold
   std::vector<int> picked;
   nms_sorted_bboxes(proposals, picked, nms_threshold);

   int count = picked.size();

   objects.resize(count);
   for (int i = 0; i < count; i++)
   {
      objects[i] = proposals[picked[i]];

      // adjust offset to original unpadded
      float x0 = (objects[i].x - (wpad / 2)) / scale;
      float y0 = (objects[i].y - (hpad / 2)) / scale;
      float x1 = (objects[i].x + objects[i].w - (wpad / 2)) / scale;
      float y1 = (objects[i].y + objects[i].h - (hpad / 2)) / scale;

      // clip

      x0 = max(min(x0, (float)(img_w - 1)), 0.f);
      y0 = max(min(y0, (float)(img_h - 1)), 0.f);
      x1 = max(min(x1, (float)(img_w - 1)), 0.f);
      y1 = max(min(y1, (float)(img_h - 1)), 0.f);

      objects[i].x = x0;
      objects[i].y = y0;
      objects[i].w = x1 - x0;
      objects[i].h = y1 - y0;
   }

   return 0;
}

static void draw_objects(const cv::Mat &bgr, const std::vector<DetectObject> &objects)
{
   static const char *class_names[] = {
       "ktp"};

   cv::Mat image = bgr.clone();

   for (size_t i = 0; i < objects.size(); i++)
   {
      const DetectObject &obj = objects[i];

      fprintf(stderr, "%d = %.5f at %.2f %.2f %.2f x %.2f\n", obj.label, obj.prob,
              obj.x, obj.y, obj.w, obj.h);

      rectangle(image, cv::Rect(obj.x, obj.y, obj.w, obj.h), cv::Scalar(255, 0, 0));

      Mat crop_img = image(Range(obj.y, obj.y + obj.h), Range(obj.x, obj.x + obj.w));
      //imwrite("crop_ktp.jpg", crop_img);

      char text[256];
      sprintf(text, "%s %.1f%%", class_names[obj.label], obj.prob * 100);

      int baseLine = 0;
      cv::Size label_size = getTextSize(text, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);

      int x = obj.x;
      int y = obj.y - label_size.height - baseLine;
      if (y < 0)
         y = 0;
      if (x + label_size.width > image.cols)
         x = image.cols - label_size.width;

      rectangle(image, cv::Rect(cv::Point(x, y), cv::Size(label_size.width, label_size.height + baseLine)),
                cv::Scalar(255, 255, 255), -1);

      putText(image, text, cv::Point(x, y + label_size.height),
              FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));
   }
   //cv::imwrite("yolov5.jpg", image);
   cv::imshow("image", image);
   //cv::waitKey(0);
}

static cv::Mat crop_objects(const cv::Mat &bgr, const std::vector<DetectObject> &objects)
{
   cv::Mat image = bgr.clone();
   const DetectObject &obj = objects[0];
   return image(Range(obj.y, obj.y + obj.h), Range(obj.x, obj.x + obj.w));
   
}

int loadImage(char *filename) {
    Mat img = imread(filename);

   int width = img.cols;
   int height = img.rows;

   bool use_gpu = false;

   if (width > 1500 || height > 1500)
   {

      float aspect_ratio = min((float)1500 / width, (float)1500 / height);

      int newWidth = floor(width * aspect_ratio);
      int newHeight = floor(height * aspect_ratio);

      resize(img, img, Size(newWidth, newHeight));
   }

   std::vector<DetectObject> objects;

   detect_yolov5(yolov5, img, objects, use_gpu);

   if (objects.size() > 0) {
      Mat crop = crop_objects(img, objects);
      imwrite("crop_ktp.jpg", crop);
      return 1;
   }

   return 0;


}

int loadModel(char *fileParam, char *fileModel)
{
   ncnn::Option opt;
   opt.lightmode = true;
   opt.num_threads = 4;
   opt.blob_allocator = &g_blob_pool_allocator;
   opt.workspace_allocator = &g_workspace_pool_allocator;
   opt.use_packing_layout = true;

   yolov5.opt = opt;

   {

      int ret = yolov5.load_param(fileParam);
      if (ret != 0)
      {
         cout << "load_param failed"  << "\n" << endl;
         return 0;
      }
   }

   {
      int ret = yolov5.load_model(fileModel);
      if (ret != 0)
      {
         cout << "load_model failed" << "\n" << endl;;
         return 0;
      }
   }

   return 1;
}

void openVideo(char *filename)
{

   /*
   
   Mat img = imread(filename);

   int width = img.cols;
   int height = img.rows;

   bool use_gpu = false;

   if (width > 1500 || height > 1500)
   {

      float aspect_ratio = min((float)1500 / width, (float)1500 / height);

      int newWidth = floor(width * aspect_ratio);
      int newHeight = floor(height * aspect_ratio);

      resize(img, img, Size(newWidth, newHeight));
   }

   ncnn::Option opt;
   opt.lightmode = true;
   opt.num_threads = 4;
   opt.blob_allocator = &g_blob_pool_allocator;
   opt.workspace_allocator = &g_workspace_pool_allocator;
   opt.use_packing_layout = true;

   

   yolov5.opt = opt;

   {

      int ret = yolov5.load_param("models/best.param");
      if (ret != 0)
      {
         std::cout << "load_param failed";
      }
   }

   {
      int ret = yolov5.load_model("models/best.bin");
      if (ret != 0)
      {
         std::cout << "load_model failed";
      }
   }

   std::vector<DetectObject> objects;

   detect_yolov5(yolov5, img, objects, use_gpu);
   draw_objects(img, objects);
   */

   
   VideoCapture vid_capture(filename);

   if (!vid_capture.isOpened())
   {
      cout << "Error opening video stream or file : " << filename << endl;
      return;
   }

   while (vid_capture.isOpened())
   {
      Mat frame;

      bool isSuccess = vid_capture.read(frame);

      if (isSuccess == true)
      {
         //imshow("Frame", frame);
         std::vector<DetectObject> objects;

         detect_yolov5(yolov5, frame, objects, false);
         draw_objects(frame, objects);
      }

      int key = waitKey(20);
      if (key == 'q')
      {
         break;
      }
   }

   vid_capture.release();
   destroyAllWindows();

   /*
   imshow("image", img);
   waitKey(0);
   */
}