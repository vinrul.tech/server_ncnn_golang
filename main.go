package main

//#cgo windows CPPFLAGS: -IC:/projects/OpenCV-MinGW-Build-OpenCV-4.5.2-x64/include
//#cgo windows CPPFLAGS: -IC:/projects/ncnn-mingw/ncnn/build/install/include/ncnn
//#cgo windows LDFLAGS: -LC:/projects/OpenCV-MinGW-Build-OpenCV-4.5.2-x64/x64/mingw/lib -lopencv_core452.dll -lopencv_highgui452.dll -lopencv_imgcodecs452.dll -lopencv_imgproc452.dll -lopencv_video452.dll -lopencv_videoio452.dll
//#cgo windows LDFLAGS: -LC:/projects/ncnn-mingw/ncnn/build/install/lib -lncnn -fopenmp
//#cgo linux LDFLAGS: -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc -lopencv_video -lopencv_videoio
//#cgo linux LDFLAGS: -lncnn
/*
#include <stdlib.h>
#include "OpenCV-implementation.h"
*/
import "C"
import (
	//"context"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

var config *viper.Viper

func init() {
	config = viper.New()
	config.SetConfigType("json")
	config.AddConfigPath("./")
	config.SetConfigName("app.config")

	err := config.ReadInConfig()
	if err != nil {
		os.Exit(0)
	}
}

func main() {

	isModelLoaded := C.loadModel(C.CString("models/best.param"), C.CString("models/best.bin"))

	if isModelLoaded == 0 {
		log.Fatalln("Model not loaded")
		return
	}

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	e.Static("/", "statics")
	e.Static("/uploads", "uploads")

	// Set renderer
	e.Renderer = echoview.Default()

	e.GET("/hello", func(c echo.Context) error {

		//return c.String(http.StatusOK, "Aku padamu")
		return c.String(http.StatusOK, "Hello")

	})

	e.GET("/", func(c echo.Context) error {
		//render with master
		return c.Render(http.StatusOK, "index", echo.Map{
			"title": "Index title!",
			"add": func(a int, b int) int {
				return a + b
			},
		})
	})

	/*
		e.POST("/upload_ktp", func(c echo.Context) error {

			form, err := c.MultipartForm()

			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
			}

			file := form.File["file"][0]

			src, err := file.Open()

			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
			}

			defer src.Close()

			fileurl := fmt.Sprintf("uploads/%s_%s", fileId, fileName)

			dst, err := os.Create(fileurl)

			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
			}

			defer dst.Close()

			// Copy
			if _, err = io.Copy(dst, src); err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
			}

			return c.JSON(http.StatusOK, constants.H{
				"filename": fileName,
			})

		})

	*/

	C.openVideo(C.CString("video.mp4"))

	//isKtpDetected := C.loadImage(C.CString("ktp.jpeg"))

	//println(isKtpDetected)

	go func() {
		port := config.GetString("port")
		if err := e.Start(port); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

	//filename := "video.mp4"
	//C.openVideo(C.CString(filename))
}
